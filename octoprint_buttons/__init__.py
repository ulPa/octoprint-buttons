# coding=utf-8
from __future__ import absolute_import

import octoprint.plugin
import RPi.GPIO as GPIO
import time


class ButtonsPlugin(
    octoprint.plugin.SettingsPlugin,
    octoprint.plugin.AssetPlugin,
    octoprint.plugin.TemplatePlugin,
    octoprint.plugin.StartupPlugin,
    octoprint.plugin.EventHandlerPlugin,
):
    def on_after_startup(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(26, GPIO.OUT)
        GPIO.setup(19, GPIO.OUT)
        GPIO.setup(13, GPIO.OUT)
        GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(20, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(21, GPIO.RISING, callback=self.handle_buttons)
        GPIO.add_event_detect(20, GPIO.RISING, callback=self.handle_buttons)
        GPIO.add_event_detect(16, GPIO.RISING, callback=self.handle_buttons)
        self.set_leds(self._printer.get_state_id())
        self.last_button_press = 0

    def on_event(self, event, payload):
        if event == "PrinterStateChanged":
            self.set_leds(payload["state_id"])

    def set_leds(self, state):
        led_states = {
            "OPERATIONAL": [1, 0, 0],
            "PRINTING": [0, 1, 1],
            "PAUSED": [1, 1, 1],
        }
        if state in led_states:
            GPIO.output(26, led_states[state][0])
            GPIO.output(19, led_states[state][1])
            GPIO.output(13, led_states[state][2])
        else:
            GPIO.output(26, 0)
            GPIO.output(19, 0)
            GPIO.output(13, 0)

    def handle_buttons(self, channel):
        if self.last_button_press+3 > time.time():
             return
        if channel == 21:
            self._printer.start_print()
        elif channel == 20:
            self._printer.toggle_pause_print()
        elif channel == 16:
            self._printer.cancel_print()
        self.last_button_press = time.time()

    def get_settings_defaults(self):
        return dict(
            Print_Led_Pin=26,
            Print_Button_Pin=21,
            Pause_Led_Pin=19,
            Pause_Button_Pin=20,
            Cancel_Led_Pin=13,
            Cancel_Button_Pin=16,
        )

    ##~~ AssetPlugin mixin

    def get_assets(self):
        # Define your plugin's asset files to automatically include in the
        # core UI here.
        return dict(
            js=["js/buttons.js"], css=["css/buttons.css"], less=["less/buttons.less"]
        )


# If you want your plugin to be registered within OctoPrint under a different name than what you defined in setup.py
# ("OctoPrint-PluginSkeleton"), you may define that here. Same goes for the other metadata derived from setup.py that
# can be overwritten via __plugin_xyz__ control properties. See the documentation for that.
__plugin_name__ = "Buttons Plugin"


# Starting with OctoPrint 1.4.0 OctoPrint will also support to run under Python 3 in addition to the deprecated
# Python 2. New plugins should make sure to run under both versions for now. Uncomment one of the following
# compatibility flags according to what Python versions your plugin supports!
# __plugin_pythoncompat__ = ">=2.7,<3" # only python 2
# __plugin_pythoncompat__ = ">=3,<4" # only python 3
__plugin_pythoncompat__ = ">=2.7,<4"  # python 2 and 3


def __plugin_load__():
    global __plugin_implementation__
    __plugin_implementation__ = ButtonsPlugin()
